# Install in a project
* Check if project already has access to the repository
    ```bash
    docker exec -it app ssh -Tv git@bitbucket.org
    ```
* Add project's ssh public key to **Access keys** list in [bitbucket](https://bitbucket.org/startechdigital/php-shopify-mock/admin/access-keys/)
* Add vcs repository to a projects **composer.json**
    ```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:startechdigital/php-shopify-mock.git"
        }
    ],
    ```
* Install package from repository
    ```bash
    docker exec -it app composer require --dev startechdigital/php-shopify-mock:dev-master
    ```

# Testing
```bash
docker-compose run composer install
docker-compose run php vendor/bin/phpunit
``` 