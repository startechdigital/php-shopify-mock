<?php

namespace PHPShopifyMock\Tests;

use PHPShopifyMock\Service;
use Orchestra\Testbench\TestCase;

class ServiceTest extends TestCase
{
    /** @test */
    public function can_init_service()
    {
        $service = new Service;
        $service->init();

        $this->assertTrue($service->isMockEnabled());
    }
}