<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class Transaction extends ShopifyResource
{
    protected $resourceKey = 'transaction';

    /**
     * Documentation
     *  https://shopify.dev/docs/admin-api/rest/reference/orders/transaction?api%5Bversion%5D=2020-04
     */

    /**
     * @param null $fulfillment
     * @param array $urlParams
     * @return array|null
     */
    public function transformOnLoad($transaction = null, array $urlParams = [])
    {
        if(!$transaction || !isset($transaction['id'])) return $transaction;

        return parent::transformOnLoad($transaction, $urlParams);
    }

    /**
     * @param array $dataArray
     * @return array
     */
    public function post(array $dataArray = []): array
    {
        $orderId = Arr::get($dataArray, 'order_id');
        $transactionId = $this->faker()->unique()->randomNumber(6, true);

        $transaction = [
            "id" => $transactionId,
            "order_id" => $orderId,
            "kind" => Arr::get($dataArray, 'kind', 'capture'),
            "gateway" => "",
            "status" => Arr::get($dataArray, 'status', 'success'),
            "message" => null,
            "created_at" => "2005-08-05T12:59:12-04:00",
            "test" => Arr::get($dataArray, 'test', false),
            "authorization" => null,
            "location_id" => null,
            "user_id" => null,
            "parent_id" => null,
            "processed_at" => "2005-08-05T12:59:12-04:00",
            "device_id" => null,
            "receipt" => [],
            "error_code" => null,
            "source_name" => "web",
            "currency_exchange_adjustment" => null,
            "amount" => Arr::get($dataArray, 'amount'),
            "currency" => Arr::get($dataArray, 'currency', 'USD'),
            "admin_graphql_api_id" => "gid://shopify/OrderTransaction/{$transactionId}"
        ];

        $this->save($transaction);

        return $this->load($transactionId);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        //filter: id
        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($image) use ($ids){
                return in_array($image['id'], $ids);
            });
        }

        //filter: order_ids
        if(($orderIds = Arr::get($urlParams, 'order_ids'))) {
            $orderIds = explode(',',(string) $orderIds);
            $storage = $storage->filter(function($image) use ($orderIds){
                return in_array($image['order_id'], $orderIds);
            });
        }

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}
