<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class Refund extends ShopifyResource
{
    protected $resourceKey = 'refund';

    /**
     * Documentation
     *  https://shopify.dev/docs/admin-api/rest/reference/orders/refund#create-2020-04
     */

    /**
     * @param null $fulfillment
     * @param array $urlParams
     * @return array|null
     */
    public function transformOnLoad($refund = null, array $urlParams = [])
    {
        if(!$refund || !isset($refund['id'])) return $refund;

        return parent::transformOnLoad($refund, $urlParams);
    }

    /**
     * @param array $dataArray
     * @return array
     */
    public function post(array $dataArray = []): array
    {
        $orderId = Arr::get($dataArray, 'order_id');
        $currency = Arr::get($dataArray, 'currency');
        $refundId = $this->faker()->unique()->randomNumber(6, true);

        $refund = [
            'id'                   => $refundId,
            'order_id'             => $orderId,
            'created_at'           => '2020-11-04T16:46:58-05:00',
            'note'                 => Arr::get($dataArray, "note"),
            'user_id'              => 799407056,
            'processed_at'         => '2020-11-04T16:46:58-05:00',
            'restock'              => true,
            'duties'               => [],
            'total_duties_set'     => [
                'shop_money'        => [
                    'amount'        => '0.00',
                    'currency_code' => $currency,
                ],
                'presentment_money' => [
                    'amount'        => '0.00',
                    'currency_code' => $currency,
                ],
            ],
            'admin_graphql_api_id' => "gid://shopify/Refund/{$refundId}",
            'refund_line_items'    => collect(Arr::get($dataArray, 'refund_line_items', []))
                ->map(function($lineItem) use ($currency) {
                    return [
                        'id'            => Arr::get($lineItem, 'line_item_id'),
                        'quantity'      => Arr::get($lineItem, 'quantity'),
                        'line_item_id'  => 703073504,
                        'location_id'   => 487838322,
                        'restock_type'  => Arr::get($lineItem, 'restock_type'),
                        'subtotal'      => 195.67,
                        'total_tax'     => 3.98,
                        'subtotal_set'  => [
                            'shop_money'        => [
                                'amount'        => '195.67',
                                'currency_code' => $currency,
                            ],
                            'presentment_money' => [
                                'amount'        => '195.67',
                                'currency_code' => $currency,
                            ],
                        ],
                        'total_tax_set' => [
                            'shop_money'        => [
                                'amount'        => '3.98',
                                'currency_code' => $currency,
                            ],
                            'presentment_money' => [
                                'amount'        => '3.98',
                                'currency_code' => $currency,
                            ],
                        ],
                        'line_item'     => [
                            'id'                           => 703073504,
                            'variant_id'                   => 457924702,
                            'title'                        => 'IPod Nano - 8gb',
                            'quantity'                     => 1,
                            'sku'                          => 'IPOD2008BLACK',
                            'variant_title'                => 'black',
                            'vendor'                       => null,
                            'fulfillment_service'          => 'manual',
                            'product_id'                   => 632910392,
                            'requires_shipping'            => true,
                            'taxable'                      => true,
                            'gift_card'                    => false,
                            'name'                         => 'IPod Nano - 8gb - black',
                            'variant_inventory_management' => 'shopify',
                            'properties'                   => [],
                            'product_exists'               => true,
                            'fulfillable_quantity'         => 1,
                            'grams'                        => 200,
                            'price'                        => '199.00',
                            'total_discount'               => '0.00',
                            'fulfillment_status'           => null,
                            'price_set'                    => [
                                'shop_money'        => [
                                    'amount'        => '199.00',
                                    'currency_code' => $currency,
                                ],
                                'presentment_money' => [
                                    'amount'        => '199.00',
                                    'currency_code' => $currency,
                                ],
                            ],
                            'total_discount_set'           => [
                                'shop_money'        => [
                                    'amount'        => '0.00',
                                    'currency_code' => $currency,
                                ],
                                'presentment_money' => [
                                    'amount'        => '0.00',
                                    'currency_code' => $currency,
                                ],
                            ],
                            'discount_allocations'         => [
                                [
                                    'amount'                     => '3.33',
                                    'discount_application_index' => 0,
                                    'amount_set'                 => [
                                        'shop_money'        => [
                                            'amount'        => '3.33',
                                            'currency_code' => $currency,
                                        ],
                                        'presentment_money' => [
                                            'amount'        => '3.33',
                                            'currency_code' => $currency,
                                        ],
                                    ],
                                ],
                            ],
                            'duties'                       => [],
                            'admin_graphql_api_id'         => 'gid://shopify/LineItem/703073504',
                            'tax_lines'                    => [
                                [
                                    'title'     => 'State Tax',
                                    'price'     => '3.98',
                                    'rate'      => 0.06,
                                    'price_set' => [
                                        'shop_money'        => [
                                            'amount'        => '3.98',
                                            'currency_code' => $currency,
                                        ],
                                        'presentment_money' => [
                                            'amount'        => '3.98',
                                            'currency_code' => $currency,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ];
                })
                ->toArray(),
            'transactions'         => [
                [
                    'id'                   => 179259969,
                    'order_id'             => 450789469,
                    'kind'                 => 'refund',
                    'gateway'              => 'bogus',
                    'status'               => 'success',
                    'message'              => null,
                    'created_at'           => '2005-08-05T12:59:12-04:00',
                    'test'                 => false,
                    'authorization'        => 'authorization-key',
                    'location_id'          => null,
                    'user_id'              => null,
                    'parent_id'            => 801038806,
                    'processed_at'         => '2005-08-05T12:59:12-04:00',
                    'device_id'            => null,
                    'receipt'              => [],
                    'error_code'           => null,
                    'source_name'          => 'web',
                    'amount'               => '209.00',
                    'currency'             => $currency,
                    'admin_graphql_api_id' => 'gid://shopify/OrderTransaction/179259969',
                ],
            ],
            'order_adjustments'    => [],
        ];

        $this->save($refund);

        return $this->load($refundId);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        //filter: id
        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($image) use ($ids){
                return in_array($image['id'], $ids);
            });
        }

        //filter: order_ids
        if(($orderIds = Arr::get($urlParams, 'order_ids'))) {
            $orderIds = explode(',',(string) $orderIds);
            $storage = $storage->filter(function($image) use ($orderIds){
                return in_array($image['order_id'], $orderIds);
            });
        }

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}
