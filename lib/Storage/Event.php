<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class Event extends ShopifyResource
{
    protected $resourceKey = 'events';

    public function post(array $dataArray = []): array
    {
        $eventId = $this->faker()->unique()->randomNumber(6, true);

        //all null fields should be passed in $data
        $event = array_merge([
            "id" => $eventId,
            "created_at" => now()->toIso8601String(),
            "author" => "Shopify",
            "path" => null,
            "body" => null,
            "subject_id"=> null,
            "subject_type" => null,
            "verb" => null,
            "arguments" => null,
            "message" => null,
            "description" => null,
        ], $dataArray);

        $this->save($event);

        return $event;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        /**
         * TODO: implement params
         *  since_id       : Show only results after the specified ID.
         *  created_at_min : Show events created at or after this date and time. (format: 2014-04-25T16:15:47-04:00)
         *  created_at_max : Show events created at or before this date and time. (format: 2014-04-25T16:15:47-04:00)
         *  filter         : Show events specified in this filter.
         *  verb           : Show events of a certain type.
         *  fields         : Show only certain fields, specified by a comma-separated list of field names.
        */

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}