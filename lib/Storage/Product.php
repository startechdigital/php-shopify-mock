<?php


namespace PHPShopifyMock\Storage;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use PHPShopify\Exception\ApiException;
use Illuminate\Support\Arr;

class Product extends ShopifyResource
{
    protected $resourceKey = 'products';

    public function transformOnLoad($product = null, array $urlParams = [])
    {
        if(!$product || !isset($product['id'])) return $product;

        /** @var Collection $variants */
        $variants = $this->getMockStorage()->productVariant->get(['product_ids' => $product['id']]);
        $images = $this->getMockStorage()->productImage->get(['product_ids' => $product['id']]);

        $product['variants'] = $variants->sortBy('position')->toArray();
        $product['images'] = $images->toArray();
        $product['image'] = $images->first();

        // sort tags
        if(($tags = Arr::get($product, 'tags', '')) && !empty(trim($tags))) {
            $tags = array_filter(array_map('trim', explode(',', $tags)));
            uasort($tags, function ($a, $b) {
                return strcmp($a, $b);
            });
            $product['tags'] = implode(', ', $tags);
        }

        return parent::transformOnLoad($product, $urlParams);
    }

    public function post(array $dataArray = []): array
    {
        $productId = $this->faker()->unique()->randomNumber(6, true);

        // set published at
        $published = Arr::get($dataArray, 'published', true);

        $product = [
            "id" => $productId,
            "title" => Arr::get($dataArray, 'title'),
            "status" => Arr::get($dataArray, 'status', 'active'),
            "body_html" => Arr::get($dataArray, 'body_html'),
            "vendor" => Arr::get($dataArray, 'vendor'),
            "product_type" => Arr::get($dataArray, 'product_type'),
            "created_at" => now()->toIso8601String(),
            "handle" => str_replace(' ', '-', strtolower(Arr::get($dataArray, 'title'))).'-'.$this->faker()->unique()->randomNumber(3, true),
            "updated_at" => now()->toIso8601String(),
            "published_at" => $published ? now()->toIso8601String() : null,
            "published_scope" => Arr::get($dataArray, 'published_scope', 'web'),
            "template_suffix" => null,
            "tags" => Arr::get($dataArray, 'tags', ''),
            "admin_graphql_api_id" => sprintf("gid://shopify/Product/%s", $productId),
        ];

        // init storage
        $this->save($product);

        // save related entities
        collect(Arr::get($dataArray, 'images', []))->each(function($image, $index) use ($productId) {
            $this->getMockStorage()->productImage->post(array_merge($image, [
                "product_id" => $productId,
                "position" => $index+1,
            ]));
        });

        // event if there are no variants sent, a default one should be created
        $variants = collect(Arr::get($dataArray, 'variants', [ [] ]))->map(function($variant, $index) use ($productId) {
            $this->getMockStorage()->productVariant->post(array_merge($variant, [
                'product_id' => $productId,
                'position' => $index+1,
            ]));
        });


        // update with default options
        $product = array_merge($product,[
            "options" => [
                [
                    "id" => $this->faker()->unique()->randomNumber(6, true),
                    "product_id" => $productId,
                    "name" => "Title",
                    "position" => 1,
                    "values" => $variants->pluck('option1')->toArray(),
                ],
            ],
        ]);
        $this->save($product);

        return $this->load($productId);
    }

    public function put($id, array $dataArray = []): array
    {
        // update published at
        $published = Arr::get($dataArray, 'published');
        if($published !== null) {
            $dataArray['published_at'] = $published ? now()->toIso8601String() : null;
        }

        // update variants
        if(($variantsData = Arr::get($dataArray, 'variants'))) {
            $existingVariantIds = $this->getMockStorage()->productVariant->getResourceStorage()->sortBy('position')->pluck('id');

            collect($variantsData)->map(function($variantData, $index) use ($id, $existingVariantIds) {
                $variantId = Arr::get($variantData, 'id');

                //try to get $variantId by index if it's not set
                if(!$variantId) $variantId = $existingVariantIds->get($index);

                //still no variant id - post the variant
                if(!$variantId) {
                    return $this->getMockStorage()->productVariant->post(array_merge($variantData, [
                        'product_id' => $id,
                        'position' => $index+1,
                    ]));
                }

                //variant id is set - put variant
                if(!$this->getMockStorage()->productVariant->getResourceStorage()->has($variantId)) {
                    throw new ApiException(sprintf(
                        'variants - The variants have not been changed. The following IDs do not exist or do not belong to the product: [%s]',
                        $variantId
                    ), 422);
                }

                return $this->getMockStorage()->productVariant->put($variantId, array_merge($variantData, [
                    'product_id' => $id,
                    'position' => $index+1,
                ]));
            });
            unset($dataArray['variants']);
        }

        return parent::put($id, $dataArray);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        // collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        // filters: id
        if(($ids = $params->get('ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($product) use ($ids){
                return in_array($product['id'], $ids);
            });
        }

        // filters: published_status
        if(($publishedStatus = $params->get('published_status', 'any')) && $publishedStatus !== 'any') {
            $storage = $storage->filter(function($product) use ($publishedStatus){
                switch($publishedStatus) {
                    case 'published':
                        return !empty(Arr::get($product, 'published_at'));
                        break;
                    case 'unpublished':
                        return empty(Arr::get($product, 'published_at'));
                        break;
                    default: break;
                }
                return true;
            });
        }

        // Show products last updated after date. (format: 2014-04-25T16:15:47-04:00)
        if(($updatedAtMin = Arr::get($urlParams, 'updated_at_min'))) {
            $updatedAtMin = Carbon::parse($updatedAtMin);
            $storage = $storage->filter(function($product) use ($updatedAtMin){
                return Carbon::parse($product['updated_at']) >= $updatedAtMin;
            });
        }


        /**
         * TODO: implement filters
         *  since_id: Restrict results to after the specified ID.
         *  title: Filter results by product title.
         *  vendor: Filter results by product vendor.
         *  handle: Filter results by product handle.
         *  product_type: Filter results by product type.
         *  collection_id: Filter results by product collection ID.
         *  created_at_min: Show products created after date. (format: 2014-04-25T16:15:47-04:00)
         *  created_at_max: Show products created before date. (format: 2014-04-25T16:15:47-04:00)
         *  updated_at_max: Show products last updated before date. (format: 2014-04-25T16:15:47-04:00)
         *  published_at_min: Show products published after date. (format: 2014-04-25T16:15:47-04:00)
         *  published_at_max: Show products published before date. (format: 2014-04-25T16:15:47-04:00)
         *  presentment_currencies: Return presentment prices in only certain currencies, specified by a comma-separated list of ISO 4217 currency codes.
         */

        // sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        // cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }

    public function delete($id)
    {
        if (!$this->load($id)) {
            return [];
        };

        $this->getMockStorage()->productVariant->get(['product_ids' => $id])->each(function($variant) {
            $this->getMockStorage()->productVariant->delete($variant['id']);
        });
        $this->getMockStorage()->productImage->get(['product_ids' => $id])->each(function($variant) {
            $this->getMockStorage()->productImage->delete($variant['id']);
        });

        parent::delete($id);

        // create destroy event for product
        $this->getMockStorage()->event->post([
            "subject_id" => $id,
            "subject_type" => "Product",
            "verb" => "destroy",
        ]);

        return [];
    }
}