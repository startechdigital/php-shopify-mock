<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class CustomCollection extends ShopifyResource
{
    protected $resourceKey = 'customCollection';

    public function post(array $dataArray = []): array
    {
        $collectionId = $this->faker()->unique()->randomNumber(6, true);

        $collection = [
            "id"                   => $collectionId,
            "handle"               => Arr::get($dataArray, 'handle'),
            "title"                => Arr::get($dataArray, 'title'),
            "body_html"            => Arr::get($dataArray, 'body_html'),
            "updated_at"           => now()->toIso8601String(),
            "published_at"         => now()->toIso8601String(),
            "sort_order"           => Arr::get($dataArray, 'sort_order'),
            "template_suffix"      => Arr::get($dataArray, 'template_suffix'),
            "published_scope"      => Arr::get($dataArray, 'published_scope', 'web'),
            "admin_graphql_api_id" => "gid://shopify/Collection/{$collectionId}",
        ];

        $this->save($collection);

        return $collection;
    }

    public function put($id, array $dataArray = []): array
    {
        return parent::put($id, $dataArray);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if ($storage->isEmpty()) {
            return collect();
        }

        // update related data
        $storage->transform(function ($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}
