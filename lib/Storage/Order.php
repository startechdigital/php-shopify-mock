<?php


namespace PHPShopifyMock\Storage;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class Order extends ShopifyResource
{
    protected $resourceKey = 'orders';

    public function transformOnLoad($order = null, array $urlParams = [])
    {
        if(!$order || !isset($order['id'])) return $order;

        $lineItems = $this->getMockStorage()->orderLineItem->get(['mock_order_ids' => $order['id']]);
        $risks = $this->getMockStorage()->orderRisk->get(['order_ids' => $order['id']]);

        // order fulfillments
        $fulfillments = $this->getMockStorage()->fulfillment->get(['order_ids' => $order['id']]);
        $order['fulfillments'] = $fulfillments->toArray();

        // order transactions
        $transactions = $this->getMockStorage()->transaction->get(['order_ids' => $order['id']]);
        $order['transactions'] = $transactions->toArray();

        // order refunds
        $refunds = $this->getMockStorage()->refund->get(['order_ids' => $order['id']]);
        $order['refunds'] = $refunds->toArray();

        //sort tags
        if(($tags = Arr::get($order, 'tags', '')) && !empty(trim($tags))) {
            $tags = array_filter(array_map('trim', explode(',', $tags)));
            uasort($tags, function ($a, $b) {
                return strcmp($a, $b);
            });
            $order['tags'] = implode(', ', $tags);
        }

        /**
         * The order's status in terms of fulfilled line items. Valid values:
         * - fulfilled: Every line item in the order has been fulfilled.
         * - null: None of the line items in the order have been fulfilled.
         * - partial: At least one line item in the order has been fulfilled.
         * - restocked: Every line item in the order has been restocked and the order canceled.
         */
        $isCancelled = (Arr::get($order, 'cancelled_at') !== null);
        $lineItemsCountByFulfillmentStatus = $lineItems->groupBy('fulfillment_status')->transform(function(Collection $group) {
            return $group->count();
        });
        $lineItemsFulfilledCount = $lineItemsCountByFulfillmentStatus->get('fulfilled');
        $lineItemsPartialCount = $lineItemsCountByFulfillmentStatus->get('partial');
        $lineItemsRestockedCount = collect(Arr::get($order, 'refunds', []))
            ->where('restock', true)
            ->transform(function(array $refund) {
               return collect(Arr::get($refund, 'refund_line_items', []))->pluck('line_item_id');
            })
            ->flatten(1)
            ->unique()
            ->count();


        $fulfillmentStatus = null;
        if($isCancelled && $lineItemsRestockedCount === $lineItems->count()) {
            $fulfillmentStatus = 'restocked';
        } else if($lineItemsFulfilledCount === $lineItems->count()) {
            $fulfillmentStatus = 'fulfilled';
        } else if ($lineItemsFulfilledCount > 0 || $lineItemsPartialCount > 0) {
            $fulfillmentStatus = 'partial';
        }

        $order['fulfillment_status'] = $fulfillmentStatus;
        $order['line_items'] = $lineItems->toArray();
        $order['risks'] = $risks->toArray();

        return parent::transformOnLoad($order, $urlParams);
    }

    public function post(array $dataArray = []): array
    {
        $orderId = Arr::get($dataArray, 'id') ?? $this->faker()->unique()->randomNumber(6, true);
        $orderNumber = Arr::get($dataArray, 'number') ?? $this->faker()->unique()->randomNumber(3, true);

        $customer = [
            'id' => 525949403187,
            'email' => null,
            'accepts_marketing' => false,
            'created_at' => '2018-04-02T19:40:10+03:00',
            'updated_at' => '2018-04-26T03:03:42+03:00',
            'first_name' => Arr::get($dataArray, 'customer.first_name', 'Joel'),
            'last_name' => Arr::get($dataArray, 'customer.last_name', 'Test'),
            'orders_count' => 4,
            'state' => 'enabled',
            'total_spent' => '110.00',
            'last_order_id' => 391629406259,
            'note' => null,
            'verified_email' => false,
            'multipass_identifier' => null,
            'tax_exempt' => false,
            'phone' => null,
            'tags' => '',
            'last_order_name' => '#1004',
            'admin_graphql_api_id' => 'gid://shopify/Customer/525949403187',
            'default_address' => Arr::get($dataArray, 'shipping_address', []),
        ];

        $lineItems = collect(Arr::get($dataArray, 'line_items', []))->map(function ($lineItem) use ($orderId) {
            $variantId = Arr::get($lineItem, 'variant_id');
            $variant = $this->getMockStorage()->productVariant->load($variantId);
            if (!$variant) return null;

            $productId = Arr::get($variant, 'product_id');
            $product = $this->getMockStorage()->product->load($productId);
            if (!$product) return null;
            $lineItem = $this->getMockStorage()->orderLineItem->post([
                'mock_order_id' => $orderId, //does not exist in shopify api (it's here only to make things work)
                'id' => $this->faker()->unique()->randomNumber(6, true),
                'variant_id' => $variantId,
                'title' => Arr::get($product, 'title'),
                'variant_title' => Arr::get($variant, 'title'),
                'name' => sprintf('%s - %s', Arr::get($product, 'title'), Arr::get($variant, 'title')),
                'quantity' => Arr::get($lineItem, 'quantity', 1),
                'price' => Arr::get($variant, 'price'),
                'sku' => Arr::get($variant, 'sku'),
                'vendor' => Arr::get($product, 'vendor', $this->faker()->word),
                'fulfillment_service' => Arr::get($variant, 'fulfillment_service', 'manual'),
                'product_id' => Arr::get($variant, 'product_id'),
                'requires_shipping' => true,
                'taxable' => false,
                'gift_card' => false,
                'variant_inventory_management' => 'shopify',
                'properties' => [],
                'product_exists' => true,
                'fulfillable_quantity' => Arr::get($lineItem, 'quantity', 1),
                'grams' => 0,
                'total_discount' => '0.00',
                'fulfillment_status' => null,
                'discount_allocations' => [],
                'admin_graphql_api_id' => 'gid://shopify/LineItem/932164468787',
                "price_set" => [
                    "shop_money" => [
                        "amount" => "0.00",
                        "currency_code" => "USD"
                    ],
                    "presentment_money" => [
                        "amount" => "0.00",
                        "currency_code" => "USD"
                    ]
                ],
                "total_discount_set" => [
                    "shop_money" => [
                        "amount" => "0.00",
                        "currency_code" => "USD"
                    ],
                    "presentment_money" => [
                        "amount" => "0.00",
                        "currency_code" => "USD"
                    ]
                ],
                "tax_lines" => [
//                    [
//                        "title" => "FL State Tax",
//                        "price" => "0.00",
//                        "rate" => 0.06,
//                        "price_set" => []
//                    ]
                ]
            ]);
            return $lineItem;
        })->filter();

        $price = round($lineItems->sum(function ($orderLineItem) {
            return $orderLineItem['price'] * $orderLineItem['quantity'];
        }), 2);
        $totalPrice = $price;
        $subtotalPrice = $price;

        $order = [
            'id' => $orderId,
            'email' => Arr::get($dataArray, 'email','itai+testing@alphadeltas.com'),
            'closed_at' => Arr::get($dataArray, 'closed_at', now()->toIso8601String()),
            'created_at' => Arr::get($dataArray, 'created_at', now()->toIso8601String()),
            'updated_at' => Arr::get($dataArray, 'updated_at', now()->toIso8601String()),
            'number' => $orderNumber,
            'note' => Arr::get($dataArray, 'note', ''),
            'token' => 'c3d32fd57d71f0930972fa21d7863a08',
            'gateway' => Arr::get($dataArray, 'gateway', 'manual'),
            'test' => Arr::get($dataArray, 'test', false),
            'total_price' => $totalPrice,
            'subtotal_price' => $subtotalPrice,
            'total_weight' => Arr::get($dataArray, 'total_weight', 0),
            'total_tax' => Arr::get($dataArray, 'total_tax', '0.00'),
            'taxes_included' => Arr::get($dataArray, 'taxes_included', false),
            'currency' => Arr::get($dataArray, 'currency', 'USD'),
            'financial_status' => Arr::get($dataArray, 'financial_status', 'pending'),
            'confirmed' => Arr::get($dataArray, 'confirmed', true),
            'total_discounts' => Arr::get($dataArray, 'total_discounts', '0.00'),
            'total_line_items_price' => Arr::get($dataArray, 'total_line_items_price', '35.00'),
            'cart_token' => Arr::get($dataArray, 'cart_token', null),
            'buyer_accepts_marketing' => Arr::get($dataArray, 'buyer_accepts_marketing', false),
            'name' => Arr::get($dataArray, 'name', "#{$orderNumber}"),
            'referring_site' => Arr::get($dataArray, 'referring_site', null),
            'landing_site' => Arr::get($dataArray, 'landing_site', null),
            'cancelled_at' => Arr::get($dataArray, 'cancelled_at', null),
            'cancel_reason' => Arr::get($dataArray, 'cancel_reason', null),
            'total_price_usd' => Arr::get($dataArray, 'total_price_usd', $totalPrice),
            'checkout_token' => Arr::get($dataArray, 'checkout_token', null),
            'reference' => Arr::get($dataArray, 'reference', null),
            'user_id' => Arr::get($dataArray, 'user_id', 9896198195),
            'location_id' => Arr::get($dataArray, 'location_id', null),
            'source_identifier' => Arr::get($dataArray, 'source_identifier', null),
            'source_url' => Arr::get($dataArray, 'source_url', null),
            'processed_at' => Arr::get($dataArray, 'processed_at', now()->toIso8601String()),
            'device_id' => Arr::get($dataArray, 'device_id', null),
            'phone' => Arr::get($dataArray, 'phone', null),
            'customer_locale' => Arr::get($dataArray, 'customer_locale', null),
            'app_id' => Arr::get($dataArray, 'app_id', 1354745),
            'browser_ip' => Arr::get($dataArray, 'browser_ip', null),
            'landing_site_ref' => Arr::get($dataArray, 'landing_site_ref', null),
            'order_number' => Arr::get($dataArray, 'order_number', $orderNumber),
            'discount_applications' => Arr::get($dataArray, 'discount_applications', []),
            'discount_codes' => Arr::get($dataArray, 'discount_codes', []),
            'note_attributes' => Arr::get($dataArray, 'note_attributes', []),
            'payment_gateway_names' => Arr::get($dataArray, 'payment_gateway_names', ['manual']),
            'processing_method' => Arr::get($dataArray, 'processing_method', 'manual'),
            'checkout_id' => Arr::get($dataArray, 'checkout_id', null),
            'source_name' => Arr::get($dataArray, 'source_name', 'shopify_draft_order'),
            'fulfillment_status' => Arr::get($dataArray, 'fulfillment_status', null),
            'tax_lines' => Arr::get($dataArray, 'tax_lines', []),
            'tags' => htmlspecialchars(Arr::get($dataArray, 'tags', '')),
            'contact_email' => Arr::get($dataArray, 'contact_email', null),
            'order_status_url' => 'https://checkout.shopify.com/1492615219/orders/c3d32fd57d71f0930972fa21d7863a08/authenticate?key=7ec3db6fdf6556dbfaa4b9148bf966ca',
            'admin_graphql_api_id' => "gid://shopify/Order/$orderId",
            'line_items' => $lineItems->toArray(),
            'shipping_lines' => Arr::get($dataArray, 'shipping_lines', []),
            'billing_address' => Arr::get($dataArray, 'billing_address', Arr::get($dataArray, 'shipping_address', [])),
            'shipping_address' => Arr::get($dataArray, 'shipping_address', []),
            'fulfillments' => Arr::get($dataArray, 'fulfillments', []),
            'transactions' => Arr::get($dataArray, 'transactions', []),
            'refunds' => Arr::get($dataArray, 'refunds', []),
            'customer' => $customer,
        ];

        $this->save($order);

        // save transaction entities
        collect(Arr::get($order, 'transactions', []))->each(function($transaction) use ($orderId) {
            $this->getMockStorage()->transaction->post(array_merge($transaction, [
                'order_id' => $orderId,
            ]));
        });

        return $order;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($order) use ($ids){
                return in_array($order['id'], $ids);
            });
        }

        // Show orders last updated at or after date (format: 2014-04-25T16:15:47-04:00).
        if(($updatedAtMin = Arr::get($urlParams, 'updated_at_min'))) {
            $updatedAtMin = Carbon::parse($updatedAtMin);
            $storage = $storage->filter(function($order) use ($updatedAtMin){
                return Carbon::parse($order['updated_at']) >= $updatedAtMin;
            });
        }

        /**
         * TODO: implement filters
         *  since_id:           Show orders after the specified ID.
         *  created_at_min:     Show orders created at or after date (format: 2014-04-25T16:15:47-04:00).
         *  created_at_max:     Show orders created at or before date (format: 2014-04-25T16:15:47-04:00).
         *  updated_at_max:     Show orders last updated at or before date (format: 2014-04-25T16:15:47-04:00).
         *  processed_at_min:   Show orders imported at or after date (format: 2014-04-25T16:15:47-04:00).
         *  processed_at_max:   Show orders imported at or before date (format: 2014-04-25T16:15:47-04:00).
         *  attribution_app_id: Show orders attributed to a certain app, specified by the app ID. Set as current to show orders for the app currently consuming the API.
         *  status:             Filter orders by their status. (default: open)
         *  financial_status:   Filter orders by their financial status. (default: any)
         *  fulfillment_status: Filter orders by their fulfillment status. (default: any)
         */

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }

    public function delete($id)
    {
        if (!$this->load($id)) {
            return [];
        };

        $this->getMockStorage()->orderLineItem->get(['mock_order_id' => $id])->each(function($orderLineItem) {
            $this->getMockStorage()->orderLineItem->delete($orderLineItem['id']);
        });
        $this->getMockStorage()->orderRisk->get(['order_id' => $id])->each(function($orderRisk) {
            $this->getMockStorage()->orderRisk->delete($orderRisk['id']);
        });

        parent::delete($id);

        //create destroy event for product
        $this->getMockStorage()->event->post([
            'subject_id' => $id,
            'subject_type' => "Order",
            'verb' => "destroy",
        ]);

        return [];
    }
}
