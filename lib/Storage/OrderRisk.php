<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class OrderRisk extends ShopifyResource
{
    protected $resourceKey = 'orderRisk';

    public function post(array $dataArray = []): array
    {
        $dataArray['id'] = $this->faker()->unique()->randomNumber(6, true);
        $this->save($dataArray);

        return $dataArray;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //filter: id
        if(($orderIds = Arr::get($urlParams, 'order_ids'))) {
            $orderIds = explode(',',(string) $orderIds);
            $storage = $storage->filter(function($risk) use ($orderIds){
                return in_array($risk['order_id'], $orderIds);
            });
        }

        //filter: risk_ids
        if(($riskIds = Arr::get($urlParams, 'risk_ids'))) {
            $riskIds = explode(',',(string) $riskIds);
            $storage = $storage->filter(function($risk) use ($riskIds){
                return in_array($risk['id'], $riskIds);
            });
        }

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }

    public function delete($id)
    {
        if (!$this->load($id)) {
            return [];
        };

        parent::delete($id);

        //create destroy event for risk
        $this->getMockStorage()->event->post([
            'subject_id' => $id,
            'subject_type' => "OrderRisk",
            'verb' => "destroy",
        ]);

        return [];
    }
}