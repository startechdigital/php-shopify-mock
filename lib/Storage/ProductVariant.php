<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class ProductVariant extends ShopifyResource
{
    protected $resourceKey = 'productVariants';

    public function transformOnLoad($productVariant = null, array $urlParams = [])
    {
        if(!$productVariant || !isset($productVariant['id'])) return $productVariant;

        $inventoryLevels = $this->getMockStorage()->inventoryLevel->get([
            'inventory_item_ids' => $productVariant['inventory_item_id'],
        ]);
        $productVariant['inventory_quantity'] = $inventoryLevels->pluck('available')->sum();

        return parent::transformOnLoad($productVariant, $urlParams);
    }

    public function post(array $dataArray = []): array
    {
        $title = implode(' / ', array_filter([
            Arr::get($dataArray, 'option1', 'Default Title'),
            Arr::get($dataArray, 'option2'),
            Arr::get($dataArray, 'option3'),
        ]));

        $id = $this->faker()->unique()->randomNumber(6, true);
        $productVariant = [
            'id' => $id,
            'product_id' => Arr::get($dataArray, 'product_id'),
            'title' => $title,
            'price' => Arr::get($dataArray, 'price', 0),
            'sku' => Arr::get($dataArray, 'sku'),
            'position' => Arr::get($dataArray, 'position', 1),
            'inventory_policy' => Arr::get($dataArray, 'inventory_policy', 'deny'),
            'compare_at_price' => Arr::get($dataArray, 'compare_at_price'),
            'fulfillment_service' => Arr::get($dataArray, 'fulfillment_service', 'manual'),
            'inventory_management' => Arr::get($dataArray, 'inventory_management'),
            'option1' => Arr::get($dataArray, 'option1'),
            'option2' => Arr::get($dataArray, 'option2'),
            'option3' => Arr::get($dataArray, 'option3'),
            'created_at' => now()->toIso8601String(),
            'updated_at' => now()->toIso8601String(),
            'taxable' => Arr::get($dataArray, 'taxable', true),
            'barcode' => Arr::get($dataArray, 'barcode'),
            'grams' => (int) Arr::get($dataArray, 'grams'),
            'image_id' => Arr::get($dataArray, 'image_id'),
            'weight' => Arr::get($dataArray, 'weight', 0.0),
            'weight_unit' => Arr::get($dataArray, 'weight_unit', 'g'),
            'inventory_item_id' => $this->faker()->unique()->randomNumber(6, true),
            'inventory_quantity' => Arr::get($dataArray, 'inventory_quantity', 0),
            'old_inventory_quantity' => Arr::get($dataArray, 'old_inventory_quantity', 0),
            'requires_shipping' => Arr::get($dataArray, 'requires_shipping', true),
            'admin_graphql_api_id' => sprintf('gid://shopify/ProductVariant/%s', $id),
        ];

        $this->save($productVariant);

        return $productVariant;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        //filter: id
        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($image) use ($ids){
                return in_array($image['id'], $ids);
            });
        }

        //filter: product_ids
        if(($productIds = Arr::get($urlParams, 'product_ids'))) {
            $productIds = explode(',',(string) $productIds);
            $storage = $storage->filter(function($image) use ($productIds){
                return in_array($image['product_id'], $productIds);
            });
        }

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}