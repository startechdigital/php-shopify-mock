<?php


namespace PHPShopifyMock\Storage;

use HansOtt\GraphQL\Query\ParserFactory;
use Illuminate\Support\Collection;
use Closure;
use Illuminate\Support\Facades\Log;

class GraphQL extends ShopifyResource
{
    protected $resourceKey = 'graphQL';

    public function setResponse(string $regexKey, Closure $closure)
    {
        $storage = $this->getResourceStorage();
        $storage->put($regexKey, $closure);
    }

    public function postGraphQL($graphQL, $variables = null): array
    {
        $storage = $this->getResourceStorage();

        if($storage->isEmpty()) {
            throw new \Exception('Shopify Mock: postGraphQL: there are no responses defined');
        }

        $closure = $storage->first(function(Closure $closure, $regexKey) use ($graphQL) {
            $match = (preg_match($regexKey, $graphQL) === 1);
            if($match) {
                Log::debug('Shopify Mock: postGraphQL: match found', [
                    'regexKey' => $regexKey,
                    'graphQL' => $graphQL,
                ]);
            }
            return $match;
        });

        if(!$closure) {
            throw new \Exception('Shopify Mock: postGraphQL: match is not found in defined responses: '.var_export($storage->keys()->toArray(), true));
        }

        if(!($closure instanceof Closure)) {
            throw new \Exception('Shopify Mock: postGraphQL: non closure set for request: '.var_export($closure, true));
        }

        return $closure($graphQL, $variables);
    }
}