<?php


namespace PHPShopifyMock\Storage;

use PHPShopifyMock\Storage;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class ShopifyResource
{
    /** @var string */
    protected $resourceKey;

    /** @var string */
    protected $resourcePrimaryKey = 'id';

    /** @var Storage */
    public $mockStorage;

    /** @var null|array */
    public $nextPageParams;

    /** @var null|array */
    public $prevPageParams;

    public function faker()
    {
        return $this->getMockStorage()->faker;
    }

    public function setMockStorage(Storage $mockStorage): self
    {
        $this->mockStorage = $mockStorage;
        return $this;
    }
    public function getMockStorage(): Storage
    {
        return $this->mockStorage;
    }


    /**
     * Get resource storage
     *
     */
    public function getResourceStorage(): Collection
    {
        $storage = $this->getMockStorage()->getScopedStorage();

        $resourceStorage = $storage->get($this->resourceKey);
        if($resourceStorage) return $resourceStorage;

        $resourceStorage = collect();
        $storage->put($this->resourceKey, $resourceStorage);

        return $resourceStorage;
    }


    public function transformOnLoad($resource = null, array $urlParams = [])
    {
        if(!$resource) return $resource;

        /**
         * fields: Retrieve only certain fields, specified by a comma-separated list of fields names.
         */
        if(($fields = Arr::get($urlParams, 'fields'))) {
            $fields = array_map('trim', explode(',',(string) $fields));
            $resource = collect($resource)->only($fields)->toArray();
        }

        return $resource;
    }

    /**
     * @param int $id
     * @param array $urlParams
     * @return array|null
     */
    public function load(int $id, array $urlParams = [])
    {
        $storage = $this->getResourceStorage();

        return $this->transformOnLoad($storage->get($id), $urlParams);
    }

    public function save(array $data = []): array
    {
        $storage = $this->getResourceStorage();

        $primary = Arr::get($data, $this->resourcePrimaryKey);
        $storage->put($primary, $data);

        return $data;
    }

    public function post(array $dataArray = []): array
    {
        return [];
    }

    public function get(array $urlParams = []): Collection
    {
        return collect();
    }

    public function put($id, array $dataArray = []): array
    {
        if(!$this->getResourceStorage()->has($id)) {
            return [];
        }

        $dataArray['updated_at'] = Carbon::now()->tz('UTC')->toIso8601String();

        return $this->save(array_replace_recursive($this->load($id), $dataArray));
    }

    public function delete($id)
    {
        if(!$this->getResourceStorage()->has($id)) {
            return [];
        }

        $this->getResourceStorage()->forget($id);
        return [];
    }

    public function count(array $urlParams = []): int
    {
        return $this->get($urlParams)->count();
    }

    /**
     * Implementation of `cursor based pagination` for get response
     *
     * @param Collection $storage
     * @param array $urlParams
     * @return Collection
     */
    public function paginateGetResponse(Collection $storage, array $urlParams): Collection
    {
        $this->nextPageParams = null;
        $this->prevPageParams = null; //currently not implemented

        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $pageInfo = json_decode(base64_decode($pageInfo), true);
        }

        //remove all list items with resourcePrimaryKey <= last_id
        if(($lastId = Arr::get($pageInfo, 'last_id'))) {
            $lastIdIndex = $storage->pluck($this->resourcePrimaryKey)->search($lastId);
            if($lastIdIndex !== false) {
                $sliceOffset = $lastIdIndex+1;
                $storage = $storage->slice($sliceOffset);
            }
        }

        // extract a slice
        $storageSlice = $storage->splice(0, Arr::get($urlParams, 'limit', 50));

        //next page: if there is anything left in storage
        if($storage->isNotEmpty()) {
            /**
             * last_id: id from the ->last() fetched record
             * last_value: time when the fetch started
             */
            $nextPageInfo = collect($urlParams)->except(['limit','fields','page_info'])
                ->when($pageInfo, function(Collection $nextPageInfo) use ($pageInfo) {
                    return $nextPageInfo->merge($pageInfo);
                })
                ->merge([
                    'last_id' => Arr::get($storageSlice->last(), $this->resourcePrimaryKey),
                    'last_value' => Carbon::now()->tz('UTC')->toDateTimeString(),
                    'direction' => 'next',
                ]);

            $this->nextPageParams = collect($urlParams)->only(['limit','fields'])
                ->put('page_info', base64_encode(json_encode($nextPageInfo)))
                ->toArray();
        }

        return $storageSlice;
    }

    /**
     * Implementation of `sorting` for get response
     *
     * @param Collection $storage
     * @param array $urlParams
     * @return Collection
     */
    public function sortGetResponse(Collection $storage, array $urlParams): Collection
    {
        $params = collect($urlParams)->only(['order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->only(['order']));
        }
        
        $sortByField = 'updated_at';
        $sortByDesc = false;
        if(($order = $params->get('order'))) {
            $explodedSortBy = explode(' ', $order, 2);
            $sortByField = strtolower(Arr::get($explodedSortBy, 0, 'updated_at'));
            $sortByDesc = (strtolower(Arr::get($explodedSortBy, 1, 'asc')) !== 'asc');
        }

        $storage = $storage->sort(function($productA, $productB) use ($sortByField, $sortByDesc) {
            $valueA = Arr::get($productA, $sortByField);
            $valueB = Arr::get($productB, $sortByField);
            if(preg_match("/^.*_at$/",$sortByField)) {
                $valueA = Carbon::parse($valueA)->timestamp;
                $valueB = Carbon::parse($valueB)->timestamp;
            }
            $result = ($valueA > $valueB);

            if($sortByDesc) $result = !$result;

            return (int) $result;
        });

        return $storage;
    }

    public function getNextPageParams()
    {
        return $this->nextPageParams;
    }

    public function getPrevPageParams()
    {
        return $this->prevPageParams;
    }
}