<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class ProductImage extends ShopifyResource
{
    protected $resourceKey = 'productImages';

    public function post(array $dataArray = []): array
    {
        $productId = Arr::get($dataArray, 'product_id');
        if(!$productId) {
            return [];
        }

        $src = Arr::get($dataArray, 'src');
        if (!$src || Str::endsWith($src, 'invalid')) {
            return [];
        }

        $nextPosition = $this->count(['product_ids' => $productId]) + 1;
        $position = (int) Arr::get($dataArray, 'position', $nextPosition);
        if($position !== $nextPosition) {
            if($position < $nextPosition) {
                //replacing: allow current image to use $position and assign image that was on that position to $nextPosition
                $this->get(['position' => $position])->each(function ($imageOnPosition) use ($productId, $nextPosition){
                    $this->put($imageOnPosition['id'], ['position' => $nextPosition]);
                });
            }
            if($position > $nextPosition) {
                $position = $nextPosition;
            }
        }

        $width  = random_int(400, 800);
        $height = random_int(400, 800);
        $src = Arr::get($dataArray, 'src', "https://picsum.photos/id/212/$width/$height");

        $productImageId = $this->faker()->unique()->randomNumber(6, true);
        $image = [
            "id" => $productImageId,
            "product_id" => $productId,
            "position" => $position,
            "created_at" => now()->toIso8601String(),
            "updated_at" => now()->toIso8601String(),
            "alt"=> null,
            "width" => $width,
            "height" => $height,
            'src' => $src,
            "variant_ids" => [],
            "admin_graphql_api_id" => sprintf("gid://shopify/ProductImage/%s", $productImageId),
        ];

        $this->save($image);

        return $image;
    }

    public function put($id, array $dataArray = []): array
    {
        // update variants
        if($variantIds = Arr::get($dataArray, 'variant_ids', [])) {
            collect($variantIds)->each(function($variantId) use ($id) {

                if(!$this->getMockStorage()->productVariant->getResourceStorage()->has($variantId)) {
                    throw new ApiException("variants - The variants have not been changed. The following IDs do not exist or do not belong to the product: {$variantId}", 422);
                }

                $this->getMockStorage()->productVariant->put($variantId, [
                    'image_id' => $id,
                ]);
            });
        }

        return parent::put($id, $dataArray);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        //filters: id
        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($image) use ($ids){
                return in_array($image['id'], $ids);
            });
        }

        //filters: product_id
        if(($productIds = Arr::get($urlParams, 'product_ids'))) {
            $productIds = explode(',',(string) $productIds);
            $storage = $storage->filter(function($image) use ($productIds){
                return in_array($image['product_id'], $productIds);
            });
        }

        //filters: position
        if(($position = Arr::get($urlParams, 'position'))) {
            $storage = $storage->where('position', $position);
        }

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->sortBy('position')->values();
    }
}
