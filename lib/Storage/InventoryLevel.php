<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class InventoryLevel extends ShopifyResource
{
    protected $resourceKey = 'inventoryLevels';
    protected $resourcePrimaryKey = 'inventory_item_id';

    public function set(array $dataArray = []): array
    {
        $inventoryItemId = Arr::get($dataArray, 'inventory_item_id', null);
        $locationId = Arr::get($dataArray, 'location_id', null);
        $inventoryLevel = [
            'inventory_item_id' => $inventoryItemId,
            'location_id' => $locationId,
            'available' => Arr::get($dataArray, 'available', false),
            'updated_at' => now()->toIso8601String(),
            'admin_graphql_api_id' => sprintf("gid://shopify/InventoryLevel/%d?inventory_item_id=%d", $locationId, $inventoryItemId),
        ];

        $this->save($inventoryLevel);

        return $inventoryLevel;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        if(($locationIds = Arr::get($urlParams, 'location_ids'))) {
            $locationIds = explode(',',(string) $locationIds);
            $storage = $storage->filter(function($productVariant) use ($locationIds){
                return in_array($productVariant['location_id'], $locationIds);
            });
        }

        if(($inventoryItemIds = Arr::get($urlParams, 'inventory_item_ids'))) {
            $inventoryItemIds = explode(',',(string) $inventoryItemIds);
            $storage = $storage->filter(function($productVariant) use ($inventoryItemIds){
                return in_array($productVariant['inventory_item_id'], $inventoryItemIds);
            });
        }

        /**
         * TODO: implement filter:
         *  updated_at_min: Show inventory levels updated at or after date (format: 2019-03-19T01:21:44-04:00).
         */

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }

    public function adjust(array $urlParams = []): array
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return [];

        $locationId = Arr::get($urlParams, 'location_id');
        $inventoryItemId = Arr::get($urlParams, 'inventory_item_id');
        $availableAdjustment = Arr::get($urlParams, 'available_adjustment');


        $level = $storage
            ->where('location_id', $locationId)
            ->where('inventory_item_id', $inventoryItemId)
            ->first();

        if(!$level) return [];

        return $this->set([
            'inventory_item_id' => $inventoryItemId,
            'location_id' => $locationId,
            'available' => $level['available'] + $availableAdjustment,
        ]);
    }
}