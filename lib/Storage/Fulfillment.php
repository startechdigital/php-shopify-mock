<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class Fulfillment extends ShopifyResource
{
    protected $resourceKey = 'fulfillments';

    /**
     * Note: Posting fulfillment it's possible to specify next structure
     * `"line_items": [ { "id": 466157049, "quantity": 1 } ]`
     *
     * if `line_items` is omitted - all line items will be fulfilled with same quantity as in in `order.line_item[].quantity`
     * if `line_items[].quantity` is omitted - specified line items will be fulfilled with same quantity as in in `order.line_item[].quantity`
     * if `line_items[].quantity` is present -
     *  quantity should be grater that 0 and less then quantity in order.line_item[].quantity
     *  `order.line_item[].fulfillment_status` will be set to 'partial'
     *
     * Documentation
     *  https://shopify.dev/docs/admin-api/rest/reference/shipping-and-fulfillment/fulfillment?api[version]=2020-04#create-2020-04
     *  https://shopify.dev/docs/admin-api/rest/reference/orders/order?api[version]=2020-04#line-items-property-2020-04
     */

    /**
     * @param null $fulfillment
     * @param array $urlParams
     * @return array|null
     */
    public function transformOnLoad($fulfillment = null, array $urlParams = [])
    {
        if(!$fulfillment || !isset($fulfillment['id'])) return $fulfillment;

        //make sure fulfillment.line_items[].quantity is the one specified on post action
        $lineItems = collect(Arr::get($fulfillment, 'mock_line_items', []))
            ->map(function(array $lineItem) {
                $orderLineItem = $this->getMockStorage()->orderLineItem->load($lineItem['id']);
                if(!$orderLineItem) return null;
                return array_merge($orderLineItem, $lineItem);
            })
            ->filter();

        $fulfillment['line_items'] = $lineItems->toArray();

        return parent::transformOnLoad($fulfillment, $urlParams);
    }

    /**
     * @param array $dataArray
     * @return array
     */
    public function post(array $dataArray = []): array
    {
        $orderId = Arr::get($dataArray, 'order_id');
        $fulfillmentId = $this->faker()->unique()->randomNumber(6, true);
        $locationId = Arr::get($dataArray, 'location_id', $this->faker()->unique()->randomNumber(6, true));

        $order = $this->getMockStorage()->order->get(['ids' => $orderId])->first();
        $orderFulfillmentCount = $this->getMockStorage()->fulfillment->get(['order_ids' => $orderId])->count();

        $fulfillmentName = sprintf('$%s.%d', $orderId, $orderFulfillmentCount+1);

        //update line item reference (posted line items only give us id & quantity)
        $mockLineItems = collect(Arr::get($dataArray, 'line_items', []));
        if($mockLineItems->isEmpty()) {
            $mockLineItems = $this->getMockStorage()->orderLineItem->get(['mock_order_id' => $orderId])
                ->whereIn('fulfillment_status', [null, 'partial']);
        }
        $mockLineItems->transform(function(array $lineItem) {
            $lineItemId = Arr::get($lineItem, 'id');
            if(!$lineItemId) return null;

            $orderLineItem = $this->getMockStorage()->orderLineItem->getResourceStorage()->get($lineItemId);
            if(!$orderLineItem) return null;

            $lineItemQuantity = Arr::get($lineItem, 'quantity');
            if(!$lineItemQuantity) {
                $lineItemQuantity = Arr::get($orderLineItem, 'quantity');
            }

            return [
                'id' => $lineItemId,
                'quantity' => $lineItemQuantity,
            ];
        })->filter();

        $trackingNumber = Arr::get($dataArray, 'tracking_number');
        $trackingNumbers = Arr::get($dataArray, 'tracking_numbers', []);
        if(count($trackingNumbers) && !$trackingNumber) $trackingNumber = $trackingNumbers[0];
        if(!count($trackingNumbers) && $trackingNumber) $trackingNumbers = [$trackingNumber];

        $trackingUrl = Arr::get($dataArray, 'tracking_url');
        $trackingUrls = Arr::get($dataArray, 'tracking_urls', []);
        if(count($trackingUrls) && !$trackingUrl) $trackingUrl = $trackingUrls[0];
        if(!count($trackingUrls) && $trackingUrl) $trackingUrls = [$trackingUrl];

        /**
         * TODO: implement
         *  The fulfillment's status depends on the line items in the order:
         *  - If the line items in the fulfillment use a manual or custom fulfillment service, then the status of the returned fulfillment will be set immediately.
         *  - If the line items use an external fulfillment service, then they will be queued for fulfillment and the status will be set to pending until the external fulfillment service has been invoked.
         */
        $status = Arr::get($dataArray, 'status', 'success');
        if($status !== 'open') {
            $status = 'success';
        }

        // all null fields should be passed in $data
        $fulfillment = [
            'id' => $fulfillmentId,
            'order_id' => Arr::get($dataArray, 'order_id'),
            'status' => Arr::get($dataArray, 'status', 'success'),
            'created_at' => now()->toIso8601String(),
            'service' => Arr::get($dataArray, 'status', 'service'),
            'updated_at' => now()->toIso8601String(),
            'tracking_company' => Arr::get($dataArray, 'tracking_company'),
            'shipment_status' => null,
            'location_id' => $locationId,
            'line_items' => [], //will be populated in self::transformOnLoad()
            'mock_line_items' => $mockLineItems->toArray(), //does not exist in shopify api (it's here only to make things work)
            'tracking_number' => $trackingNumber,
            'tracking_numbers' => $trackingNumbers,
            'tracking_url' => $trackingUrl,
            'tracking_urls' => $trackingUrls,
            'receipt' => [],
            'name' => $fulfillmentName,
            'admin_graphql_api_id' =>  sprintf('gid://shopify/Fulfillment/%s', $fulfillmentId),
        ];

        $this->save($fulfillment);

        //trigger order line items update
        $this->getMockStorage()->orderLineItem->onFulfillmentUpdate($mockLineItems->pluck('id'));

        return $this->load($fulfillmentId);
    }

    public function put($id, array $dataArray = []): array
    {
        $trackingNumber = Arr::get($dataArray, 'tracking_number');
        $trackingNumbers = Arr::get($dataArray, 'tracking_numbers', []);
        if(count($trackingNumbers) && !$trackingNumber) $trackingNumber = $trackingNumbers[0];
        if(!count($trackingNumbers) && $trackingNumber) $trackingNumbers = [$trackingNumber];

        $trackingUrl = Arr::get($dataArray, 'tracking_url');
        $trackingUrls = Arr::get($dataArray, 'tracking_urls', []);
        if(count($trackingUrls) && !$trackingUrl) $trackingUrl = $trackingUrls[0];
        if(!count($trackingUrls) && $trackingUrl) $trackingUrls = [$trackingUrl];

        $dataArray = array_merge($dataArray, [
            'tracking_number' => $trackingNumber,
            'tracking_numbers' => $trackingNumbers,
            'tracking_url' => $trackingUrl,
            'tracking_urls' => $trackingUrls,
        ]);

        $fulfillment = parent::put($id, $dataArray);

        if(!($id = Arr::get($fulfillment, 'id'))) {
            return $fulfillment;
        }

        //trigger order line items update
        $lineItems = collect(Arr::get($fulfillment, 'mock_line_items', []));
        $this->getMockStorage()->orderLineItem->onFulfillmentUpdate($lineItems->pluck('id'));

        return $this->load($id);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        //collect params
        $params = collect($urlParams)->except(['page_info','fields', 'limit', 'order']);
        if(($pageInfo = Arr::get($urlParams, 'page_info'))) {
            $params = $params->merge(collect(json_decode(base64_decode($pageInfo), true))->except(['last_id','last_value','direction']));
        }

        //filter: order_ids (not an actual filter, but it's needed for internal logic)
        if(($orderIds = Arr::get($urlParams, 'order_ids'))) {
            $orderIds = explode(',',(string) $orderIds);
            $storage = $storage->filter(function($fulfillment) use ($orderIds){
                return in_array($fulfillment['order_id'], $orderIds);
            });
        }

        /**
         * TODO: implement params
         *  created_at_max  : Show fulfillments created before date (format: 2014-04-25T16:15:47-04:00).
         *  created_at_min  : Show fulfillments created after date (format: 2014-04-25T16:15:47-04:00).
         *  fields          : A comma-separated list of fields to include in the response.
         *  limit           : Limit the amount of results. (default: 50, maximum: 250)
         *  since_id        : Restrict results to after the specified ID.
         *  updated_at_max  : Show fulfillments last updated before date (format: 2014-04-25T16:15:47-04:00).
         *  updated_at_min  : Show fulfillments last updated after date (format: 2014-04-25T16:15:47-04:00).
        */

        //sorting
        $storage = $this->sortGetResponse($storage, $urlParams);

        //cursor based pagination (original keys are lost after it's done)
        $storage = $this->paginateGetResponse($storage, $urlParams);

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}