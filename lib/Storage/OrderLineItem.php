<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class OrderLineItem extends ShopifyResource
{
    protected $resourceKey = 'orderLineItems';

    /**
     * NOTE: Current class is made only to simplify internal logic of the library
     * PHPShopify\ShopifySDK does not have a resource for OrderLineItem
     * Shopify REST API also does not have endpoints to access Order LineItems directly
     */

    public function post(array $dataArray = []): array
    {
        $this->save($dataArray);

        return $dataArray;
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if($storage->isEmpty()) return collect();

        if(($ids = Arr::get($urlParams, 'ids'))) {
            $ids = explode(',',(string) $ids);
            $storage = $storage->filter(function($orderLineItem) use ($ids){
                return in_array($orderLineItem['id'], $ids);
            });
        }

        if(($mockOrderIds = Arr::get($urlParams, 'mock_order_ids'))) {
            $mockOrderIds = explode(',',(string) $mockOrderIds);
            $storage = $storage->filter(function($orderLineItem) use ($mockOrderIds){
                return in_array($orderLineItem['mock_order_id'], $mockOrderIds);
            });
        }

        // update related data
        $storage->transform(function($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }

    public function onFulfillmentUpdate(Collection $ids)
    {
        $lineItems = $this->getResourceStorage()->whereIn('id', $ids);
        $lineItems->each(function($lineItem) {
            $this->updateFulfillmentProperties($lineItem);
        });
    }

    /**
     * Update the amount that is available to fulfill for line item
     * @param array $lineItem
     */
    public function updateFulfillmentProperties(array $lineItem)
    {
        $lineItemId = Arr::get($lineItem, 'id');
        $orderId = Arr::get($lineItem, 'mock_order_id');
        $order = $this->getMockStorage()->order->getResourceStorage()->get($orderId); //load without transformations

        $quantity = collect([
            'ordered' => Arr::get($lineItem, 'quantity'),
            'fulfilled' => 0,
            'pendingFulfilled' => 0,
            'openFulfilled' => 0,
            'refunded' => 0,
        ]);

        $this->getMockStorage()->fulfillment->getResourceStorage()
            ->where('order_id', $orderId)
            ->each(function(array $fulfillment) use ($lineItemId, $quantity) {
                $fulfillmentLineItem = collect(Arr::get($fulfillment, 'mock_line_items', []))
                    ->firstWhere('id', $lineItemId);
                if(!$fulfillmentLineItem) return;

                $fulfillmentLineItemQuantity = Arr::get($fulfillmentLineItem, 'quantity');

                $fulfillmentStatus = Arr::get($fulfillment, 'status');
                $fulfillmentStatusToQuantityKey = [
                    'success' => 'fulfilled',
                    'pending' => 'pendingFulfilled',
                    'open' => 'openFulfilled',
                ];
                $quantityKey = Arr::get($fulfillmentStatusToQuantityKey, $fulfillmentStatus);
                if(!$quantityKey) return;

                $quantity->put($quantityKey, $quantity->get($quantityKey) + $fulfillmentLineItemQuantity);
            });


        /**
         * TODO: implement line items restock quantity count
         * Should do something like this
         *
         * collect(Arr::get($order, 'refunds', []))
         *  ->where('restock', true)
         *  ->each(function(array $refund) {
         *      $refundedLineItems = collect(Arr::get($refund, 'refund_line_items', []));
         *      ...
         *      $quantity->put('refunded', $quantity->get('refunded') + $refundedLineItemQuantity);
         *  });
         */


        /**
         * fulfillable_quantity = quantity - max(refunded_quantity, fulfilled_quantity) - pending_fulfilled_quantity - open_fulfilled_quantity
         * Documentation: https://shopify.dev/docs/admin-api/rest/reference/orders/order?api[version]=2020-04#line-items-property-2020-04
         */
        $fulfillableQuantity = $quantity->get('ordered') - max($quantity->get('refunded'), $quantity->get('fulfilled')) - $quantity->get('pendingFulfilled') - $quantity->get('openFulfilled');

        /**
         * fulfillment_status: null, fulfilled, partial, and not_eligible
         */
        $fulfillmentStatus = null;
        if($quantity->get('ordered') === $quantity->get('fulfilled')) {
            $fulfillmentStatus = 'fulfilled';
        } else if ($quantity->get('fulfilled') > 0 && $quantity->get('ordered') > $quantity->get('fulfilled')) {
            $fulfillmentStatus = 'partial';
        }

        $this->put($lineItemId, [
            'fulfillable_quantity' => $fulfillableQuantity,
            'fulfillment_status' => $fulfillmentStatus,
        ]);
    }
}