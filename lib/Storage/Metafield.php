<?php


namespace PHPShopifyMock\Storage;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class Metafield extends ShopifyResource
{
    protected $resourceKey = 'metafield';

    public function post(array $dataArray = []): array
    {
        $metafieldId = $this->faker()->unique()->randomNumber(6, true);

        $metafield = [
            "id"                   => $metafieldId,
            "namespace"            => Arr::get($dataArray, 'namespace'),
            "key"                  => Arr::get($dataArray, 'key'),
            "value"                => Arr::get($dataArray, 'value'),
            "value_type"           => Arr::get($dataArray, 'value_type'),
            "description"          => null,
            "owner_id"             => $this->faker()->unique()->randomNumber(6, true),
            "created_at"           => now()->toIso8601String(),
            "updated_at"           => now()->toIso8601String(),
            "owner_resource"       => 'product',
            "admin_graphql_api_id" => "gid://shopify/Metafield/{$metafieldId}",
        ];

        $this->save($metafield);

        return $metafield;
    }

    public function put($id, array $dataArray = []): array
    {
        return parent::put($id, $dataArray);
    }

    public function get(array $urlParams = []): Collection
    {
        $storage = $this->getResourceStorage();
        if ($storage->isEmpty()) {
            return collect();
        }

        // update related data
        $storage->transform(function ($item) use ($urlParams) {
            return $this->transformOnLoad($item, $urlParams);
        });

        return $storage->values();
    }
}
