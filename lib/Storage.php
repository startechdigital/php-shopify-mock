<?php

namespace PHPShopifyMock;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use PHPShopify\ShopifySDK;
use Illuminate\Support\Collection;
use PHPShopifyMock\Storage\Event;
use PHPShopifyMock\Storage\InventoryLevel;
use PHPShopifyMock\Storage\Metafield;
use PHPShopifyMock\Storage\Order;
use PHPShopifyMock\Storage\OrderLineItem;
use PHPShopifyMock\Storage\Fulfillment;
use PHPShopifyMock\Storage\OrderRisk;
use PHPShopifyMock\Storage\Product;
use PHPShopifyMock\Storage\ProductImage;
use PHPShopifyMock\Storage\ProductVariant;
use PHPShopifyMock\Storage\ShopifyResource;
use PHPShopifyMock\Storage\GraphQL;
use PHPShopifyMock\Storage\Transaction;
use PHPShopifyMock\Storage\Refund;
use PHPShopifyMock\Storage\CustomCollection;

/**
 * @property-read Product $product
 * @property-read ProductImage $productImage
 * @property-read ProductVariant $productVariant
 * @property-read Event $event
 * @property-read Order $order
 * @property-read OrderRisk $orderRisk
 * @property-read OrderLineItem $orderLineItem
 * @property-read InventoryLevel $inventoryLevel
 * @property-read Fulfillment $fulfillment
 * @property-read Transaction $transaction
 * @property-read Refund $refund
 *
 * Class Storage
 * @package PHPShopifyMock
 */
class Storage
{
    /** @var array */
    public $resources;

    /** @var FakerGenerator */
    public $faker;

    /** @var Collection */
    public $storage;

    public function __construct()
    {
        $this->resources = collect([
            'product' => Product::class,
            'productImage' => ProductImage::class,
            'productVariant' => ProductVariant::class,
            'event' => Event::class,
            'order' => Order::class,
            'orderRisk' => OrderRisk::class,
            'orderLineItem' => OrderLineItem::class,
            'inventoryLevel' => InventoryLevel::class,
            'fulfillment' => Fulfillment::class,
            'graphQL' => GraphQL::class,
            'transaction' => Transaction::class,
            'refund' => Refund::class,
            'metafield' => Metafield::class,
            'customCollection' => CustomCollection::class,
        ])->transform(function($resourceClass) {
            return (new $resourceClass)->setMockStorage($this);
        });

        $this->faker = FakerFactory::create(FakerFactory::DEFAULT_LOCALE);

        $this->storage = collect();
    }

    public function getScopeKey(): string
    {
        return ShopifySDK::$config['ShopUrl'];
    }

    /**
     * Get scoped storage
     */
    public function getScopedStorage(): Collection
    {
        $storage = $this->storage->get($this->getScopeKey());
        if($storage) return $storage;

        $storage = collect();
        $this->storage->put($this->getScopeKey(), $storage);

        return $storage;
    }

    public function __call($resourceName, $arguments)
    {
        $resource = $this->resources->get($resourceName);
        if(!$resource) {
            throw new \Exception("Shopify Mock: Storage: {$resourceName} is not implemented");
        }

        //Initiate the resource object
        return $resource;
    }

    /**
     * Return ShopifyResource instance
     *
     * @param string $resourceName
     *
     * @return ShopifyResource
     *
     * @example $storage->product;
     * Will call $storage->product(null)
     *
     */
    public function __get($resourceName)
    {
        return $this->$resourceName();
    }
}
