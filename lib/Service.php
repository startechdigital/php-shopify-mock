<?php


namespace PHPShopifyMock;

use PHPShopify\ShopifySDK;
use PHPShopifyMock\Service\ShopifySDK as MockShopifySDK;
use Illuminate\Support\Facades\Log;

class Service
{
    /** @var Storage */
    public $storage;

    /** @var bool */
    public $enabled = true;

    public function __construct()
    {
        $this->storage = new Storage();
    }

    public function isMockEnabled(): bool
    {
        return app()->bound(ShopifySDK::class);
    }

    public function init() {
        /**
         * config option to disable shopify mock
         * if set to FALSE this init won't do any thing - providing real API interaction where mock should be used
         */
        if(!$this->enabled) {
            Log::debug('Shopify Mock is disabled, using real API');
            return;
        }

        //check if ShopifySDK was already bound to a Mock class
        if($this->isMockEnabled()) {
            Log::debug('Shopify Mock is already initialized');
            return;
        }

        Log::debug('Shopify Mock is enabled');

        app()->bind(ShopifySDK::class, function ($app, array $config = []) {
            return (new MockShopifySDK($config))->setMockStorage($this->storage);
        });
    }

    public function remove() {
        if(!$this->isMockEnabled()) return;

        app()->offsetUnset(ShopifySDK::class);
    }
}