<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use Illuminate\Support\Facades\Log;
use PHPShopify\GraphQL as OriginalGraphQL;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class GraphQL extends OriginalGraphQL
{
    use ShopifyResourceTrait;

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->graphQL;
    }

    /**
     * @inheritDoc
     */
    public function post($graphQL, $url = null, $wrapData = false, $variables = null)
    {
        Log::debug("Shopify Mock: GraphQL: post", [
            'graphQL' => $graphQL,
            'variables' => $variables,
        ]);

        return $this->getMockStorageResource()->postGraphQL($graphQL, $variables);
    }

}