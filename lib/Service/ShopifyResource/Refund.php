<?php

namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\Refund as RefundFulfillment;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;
use Illuminate\Support\Arr;

class Refund extends RefundFulfillment
{
    use ShopifyResourceTrait {
        mockPost as basicMockPost;
        mockCount as basicMockCount;
        mockGet as basicMockGet;
    }

    /**
     * @return MockStorageShopifyResource
     */
    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->refund;
    }

    /**
     * @throws ApiException
     */
    public function mockValidateParentResource()
    {
        $order = $this->getMockParentResource();
        if ($order && (!$order->id || !$this->getMockStorage()->order->load($order->id))) {
            throw new ApiException('Not Found');
        };
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if ($order = $this->getMockParentResource()) {
            $urlParams['order_ids'] = $order->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if ($order = $this->getMockParentResource()) {
            $dataArray['order_id'] = $order->id;
        }

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }

    public function mockValidateWriteRequest(array $dataArray = [])
    {
        //$transaction = $this->getMockStorageResource()->getResourceStorage()->get($this->id, []);
        //
        //$availableKindUpdates = ['authorization', 'capture', 'sale', 'void', 'refund'];
        //
        //$currentKind = Arr::get($transaction, 'kind');
        //$updatedKind = Arr::get($dataArray, 'kind');
        //
        //if ($currentKind && $updatedKind && $currentKind !== $updatedKind) {
        //    if (!in_array($updatedKind, $availableKindUpdates)) {
        //        throw new ApiException('An error occurred, please try again');
        //    }
        //}
    }
}
