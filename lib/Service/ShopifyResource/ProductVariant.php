<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\ProductVariant as OriginalProductVariant;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;
use Illuminate\Support\Arr;

class ProductVariant extends OriginalProductVariant
{
    use ShopifyResourceTrait {
        mockGet as basicMockGet;
        mockPost as basicMockPost;
        mockCount as basicMockCount;
    }

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->productVariant;
    }

    public function mockValidateParentResource()
    {
        $product = $this->getMockParentResource();
        if ($product && (!$product->id || !$this->getMockStorage()->product->load($product->id))) {
            throw new ApiException('Invalid Product', 404);
        };
    }

    public function mockValidateWriteRequest(array $dataArray = [])
    {
        if(Arr::get($dataArray,'inventory_quantity') || Arr::get($dataArray, 'inventory_quantity_adjustment')) {
            throw new ApiException('Write requests to inventory_quantity and inventory_quantity_adjustment are no longer supported. Please use the Inventory Levels API.');
        }

        if(Arr::has($dataArray,'price') && is_null(Arr::get($dataArray,'price'))) {
            throw new ApiException("price - can't be blank");
        }
    }
    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if (($product = $this->getMockParentResource())) {
            $urlParams['product_ids'] = $product->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if (($product = $this->getMockParentResource())) {
            $dataArray['product_id'] = $product->id;
        }

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }

    public function mockCount($urlParams)
    {
        if ($product = $this->getMockParentResource()) {
            $urlParams['product_ids'] = $product->id;
        }

        return $this->basicMockCount($urlParams);
    }
}