<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Event as OriginalEvent;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class Event extends OriginalEvent
{
    use ShopifyResourceTrait;

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->event;
    }
}