<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\Metafield as OriginalMetafield;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class Metafield extends OriginalMetafield
{
    use ShopifyResourceTrait {
        mockGet as basicMockGet;
        mockPost as basicMockPost;
        mockPut as basicMockPut;
    }

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->metafield;
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if (($metafield = $this->getMockParentResource())) {
            $urlParams['id'] = $metafield->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if (($metafield = $this->getMockParentResource())) {
            $dataArray['id'] = $metafield->id;
        }

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }
}
