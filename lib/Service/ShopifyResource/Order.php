<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Order as OriginalOrder;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;
use Illuminate\Support\Arr;

class Order extends OriginalOrder
{
    use ShopifyResourceTrait;

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->order;
    }

    public function mockCancel($dataArray)
    {
        //TODO: process deprecated Arr::get($dataArray, 'restock')
        //TODO: create refund
        $orderData = [
            'cancel_reason' => Arr::get($dataArray, 'reason', 'other'),
            'cancelled_at' => now()->toIso8601String(),
        ];
        return $this->mockPut($orderData);
    }

    /**
     * TODO: implement validation
     * line_items - expected Hash to be a Array (line_items - should be a "numeric array")
     *
     * TODO: implement custom post actions
     * close
     * open
     */
}
