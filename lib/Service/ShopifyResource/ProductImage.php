<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\ProductImage as OriginalProductImage;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class ProductImage extends OriginalProductImage
{
    use ShopifyResourceTrait {
        mockGet as basicMockGet;
        mockPost as basicMockPost;
        mockCount as basicMockCount;
    }

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->productImage;
    }

    public function mockValidateParentResource()
    {
        $product = $this->getMockParentResource();
        if ($product && (!$product->id || !$this->getMockStorage()->product->load($product->id))) {
            throw new ApiException('Invalid Product', 404);
        };
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if (($product = $this->getMockParentResource())) {
            $urlParams['product_ids'] = $product->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if (($product = $this->getMockParentResource())) {
            $dataArray['product_id'] = $product->id;
        }

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }

    public function mockCount($urlParams)
    {
        if (($product = $this->getMockParentResource())) {
            $urlParams['product_ids'] = $product->id;
        }

        return $this->basicMockCount($urlParams);
    }
}