<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\OrderRisk as OriginalOrderRisk;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class OrderRisk extends OriginalOrderRisk
{
    use ShopifyResourceTrait {
        mockGet as basicMockGet;
        mockPost as basicMockPost;
        mockCount as basicMockCount;
    }

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->orderRisk;
    }

    public function mockValidateParentResource()
    {
        $order = $this->getMockParentResource();
        if ($order && (!$order->id || !$this->getMockStorage()->order->load($order->id))) {
            throw new ApiException('Invalid Order', 404);
        };
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if (($order = $this->getMockParentResource())) {
            $urlParams['order_ids'] = $order->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if (($order = $this->getMockParentResource())) {
            $dataArray['order_id'] = $order->id;
        }

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }

    public function mockCount($urlParams)
    {
        if ($order = $this->getMockParentResource()) {
            $urlParams['order_ids'] = $order->id;
        }

        return $this->basicMockCount($urlParams);
    }
}