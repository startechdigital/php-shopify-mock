<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\InventoryLevel as OriginalInventoryLevel;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class InventoryLevel extends OriginalInventoryLevel
{
    use ShopifyResourceTrait;

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->inventoryLevel;
    }

    public function mockSet($dataArray)
    {
        $this->mockValidateParentResource();
        return $this->getMockStorageResource()->set($dataArray);
    }

    public function mockAdjust($dataArray)
    {
        $this->mockValidateParentResource();
        return $this->getMockStorageResource()->adjust($dataArray);
    }

    /**
     * TODO: implement custom post actions
     *
     *  connect
     */
}