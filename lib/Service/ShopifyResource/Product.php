<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Product as OriginalProduct;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;

class Product extends OriginalProduct
{
    use ShopifyResourceTrait;

    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->product;
    }
}