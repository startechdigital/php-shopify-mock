<?php


namespace PHPShopifyMock\Service\ShopifyResource;

use PHPShopify\Exception\ApiException;
use PHPShopify\Fulfillment as OriginalFulfillment;
use PHPShopifyMock\Service\ShopifyResourceTrait;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;
use Illuminate\Support\Arr;

class Fulfillment extends OriginalFulfillment
{
    use ShopifyResourceTrait {
        mockPost as basicMockPost;
        mockCount as basicMockCount;
        mockGet as basicMockGet;
    }
    /**
     * @return MockStorageShopifyResource
     */
    public function getMockStorageResource(): MockStorageShopifyResource
    {
        return $this->getMockStorage()->fulfillment;
    }

    /**
     * @throws ApiException
     */
    public function mockValidateParentResource()
    {
        $order = $this->getMockParentResource();
        if ($order && (!$order->id || !$this->getMockStorage()->order->load($order->id))) {
            throw new ApiException('Not Found', 404);
        };
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        if (($order = $this->getMockParentResource())) {
            $urlParams['order_ids'] = $order->id;
        }

        return $this->basicMockGet($urlParams, $url, $dataKey);
    }

    public function mockPost($dataArray, $url = null, $wrapData = true)
    {
        if (($order = $this->getMockParentResource())) {
            $dataArray['order_id'] = $order->id;
        }

        // TODO: add a check for $lineItems->isEmpty()
        // TODO: add a check All line items being fulfilled must have the same fulfillment service.

        return $this->basicMockPost($dataArray, $url, $wrapData);
    }

    public function mockCount($urlParams)
    {
        if (($order = $this->getMockParentResource())) {
            $urlParams['order_id'] = $order->id;
        }

        return $this->basicMockCount($urlParams);
    }

    public function mockValidateWriteRequest(array $dataArray = [])
    {
        $fulfillment = $this->getMockStorageResource()->getResourceStorage()->get($this->id, []);

        // validate status update requests: open|complete|cancel
        $availableStatusUpdates = [
            'pending' => ['open', 'success', 'cancelled', 'error', 'failed'],
            'open' => ['success', 'cancelled', 'error', 'failed'],
            'success' => ['cancelled'],
            'cancelled' => [],
            'error' => [],
            'failed' => [],
        ];
        $currentStatus = Arr::get($fulfillment, 'status');
        $updatedStatus = Arr::get($dataArray,'status');
        if($currentStatus && $updatedStatus && $currentStatus !== $updatedStatus) {
            if(!in_array($updatedStatus, Arr::get($availableStatusUpdates, $currentStatus, []))) {
                throw new ApiException('An error occurred, please try again');
            }
        }

        /**
         * TODO: after inventory location is implemented, add location check
         *  ```
         *  $locationId = Arr::get($dataArray, 'location_id');
         *  if($locationId && !$this->getMockStorage()->locations->load($locationId)) {
         *      throw new ApiException('Not Found', 404);
         *  }
         *  ```
         */
    }

    public function mockOpen($dataArray, $url = null, $wrapData = true)
    {
        return $this->mockPut(['status' => 'open']);
    }

    public function mockComplete($dataArray, $url = null, $wrapData = true)
    {
        return $this->mockPut(['status' => 'success']);
    }

    public function mockCancel($dataArray, $url = null, $wrapData = true)
    {
        return $this->mockPut(['status' => 'cancelled']);
    }

    /**
     * TODO: implement custom post actions
     *  update_tracking
     */

}