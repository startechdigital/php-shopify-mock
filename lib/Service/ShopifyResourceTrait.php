<?php


namespace PHPShopifyMock\Service;

use PHPShopify\Exception\ApiException;
use PHPShopify\Exception\CurlException;
use PHPShopify\Exception\SdkException;
use PHPShopify\HttpRequestJson;
use PHPShopify\ShopifyResource;
use PHPShopifyMock\Storage;
use PHPShopifyMock\Storage\ShopifyResource as MockStorageShopifyResource;
use Illuminate\Support\Facades\Log;

trait ShopifyResourceTrait
{
    /** @var Storage */
    public $mockStorage;

    /** @var ShopifyResource */
    public $mockParentResource;

    public function setMockStorage(Storage $mockStorage): self
    {
        $this->mockStorage = $mockStorage;
        return $this;
    }

    public function getMockStorage(): Storage
    {
        return $this->mockStorage;
    }

    public function setMockParentResource(ShopifyResource $mockParentResource): self
    {
        $this->mockParentResource = $mockParentResource;
        return $this;
    }

    /**
     * @return ShopifyResource|null
     */
    public function getMockParentResource()
    {
        return $this->mockParentResource;
    }

    /* Override Base Functions */

    /**
     * Make sure we create instance of mock resources whenever we can
     * @inheritDoc
     */
    public function __call($name, $arguments)
    {
        //If the $name starts with an uppercase letter, it's considered as a child class
        //Otherwise it's a custom action
        if (ctype_upper($name[0])) {
            //Get the array key of the childResource in the childResource array
            $childKey = array_search($name, $this->childResource);

            if ($childKey === false) {
                throw new SdkException("Child Resource $name is not available for " . $this->getResourceName());
            }

            //If any associative key is given to the childname, then it will be considered as the class name,
            //otherwise the childname will be the class name
            $childClassName = !is_numeric($childKey) ? $childKey : $name;


            $childClass = __NAMESPACE__ . "\\ShopifyResource\\$childClassName";
            $useShopifyMock = class_exists($childClass);

            if(!$useShopifyMock) {
                Log::debug("Shopify Mock: child resource {$childClassName} is not implemented for {$this->getResourceName()}, using real one");
                $childClass = __NAMESPACE__ . "\\" . $childClassName;
            }

            //If first argument is provided, it will be considered as the ID of the resource.
            $resourceID = !empty($arguments) ? $arguments[0] : null;
            $api = new $childClass($resourceID, $this->resourceUrl);

            //set MockStorage and parent if we are using mocked resource
            if($useShopifyMock) {
                $api->setMockStorage($this->getMockStorage())->setMockParentResource($this);
            }

            return $api;
        } else {
            $actionMaps = [
                'post'  =>  'customPostActions',
                'put'   =>  'customPutActions',
                'get'   =>  'customGetActions',
                'delete'=>  'customDeleteActions',
            ];

            //Get the array key for the action in the actions array
            foreach ($actionMaps as $httpMethod => $actionArrayKey) {
                $actionKey = array_search($name, $this->$actionArrayKey);
                if ($actionKey !== false) break;
            }

            if ($actionKey === false) {
                throw new SdkException("No action named $name is defined for " . $this->getResourceName());
            }

            //If any associative key is given to the action, then it will be considered as the method name,
            //otherwise the action name will be the method name
            $customAction = !is_numeric($actionKey) ? $actionKey : $name;

            //Get the first argument if provided with the method call
            $methodArgument = !empty($arguments) ? $arguments[0] : [];

            $mockCustomAction = 'mock'.ucfirst($customAction);
            if(method_exists($this, $mockCustomAction)) {
                return $this->{$mockCustomAction}($methodArgument);
            }

            Log::debug("Shopify Mock: custom action {$mockCustomAction} is not implemented for {$this->getResourceName()}, using real one");

            //Url parameters
            $urlParams = [];
            //Data body
            $dataArray = [];

            //Consider the argument as url parameters for get and delete request
            //and data array for post and put request
            if ($httpMethod == 'post' || $httpMethod == 'put') {
                $dataArray = $methodArgument;
            } else {
                $urlParams =  $methodArgument;
            }

            $url = $this->generateUrl($urlParams, $customAction);

            if ($httpMethod == 'post' || $httpMethod == 'put') {
                return $this->$httpMethod($dataArray, $url, false);
            } else {
                return $this->$httpMethod($dataArray, $url);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function get($urlParams = [], $url = null, $dataKey = null)
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: get", [
            'id' => $this->id,
            'urlParams' => $urlParams,
            'url' => $url,
            'dataKey' => $dataKey,
        ]);

        if(method_exists($this,'mockGet')) {
            return $this->mockGet($urlParams, $url, $dataKey);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: get is not implemented, using real one");

        return parent::get($urlParams, $url, $dataKey);
    }

    /**
     * @inheritDoc
     */
    public function count($urlParams = [])
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: count", [
            'id' => $this->id,
            'urlParams' => $urlParams,
        ]);

        if(method_exists($this,'mockCount')) {
            return $this->mockCount($urlParams);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: count is not implemented, using real one");

        return parent::count($urlParams);
    }

    /**
     * @inheritDoc
     */
    public function search($query)
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: search", [
            'id' => $this->id,
            'query' => $query,
        ]);

        if(method_exists($this,'mockSearch')) {
            return $this->mockSearch($query);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: search is not implemented, using real one");

        return parent::search($query);
    }

    /**
     * @inheritDoc
     */
    public function post($dataArray, $url = null, $wrapData = true)
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: post", [
            'id' => $this->id,
            'dataArray' => $dataArray,
            'url' => $url,
            'wrapData' => $wrapData,
        ]);

        if(method_exists($this,'mockPost')) {
            return $this->mockPost($dataArray, $url, $wrapData);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: post is not implemented, using real one");

        return parent::post($dataArray, $url, $wrapData);
    }

    /**
     * @inheritDoc
     */
    public function put($dataArray, $url = null, $wrapData = true)
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: put", [
            'id' => $this->id,
            'dataArray' => $dataArray,
            'url' => $url,
            'wrapData' => $wrapData,
        ]);

        if(method_exists($this,'mockPut')) {
            return $this->mockPut($dataArray, $url, $wrapData);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: put is not implemented, using real one");

        return parent::put($dataArray, $url, $wrapData);
    }

    /**
     * @inheritDoc
     */
    public function delete($urlParams = [], $url = null)
    {
        Log::debug("Shopify Mock: {$this->resourceKey}: delete", [
            'id' => $this->id,
            'urlParams' => $urlParams,
            'url' => $url,
        ]);

        if(method_exists($this,'mockDelete')) {
            return $this->mockDelete($urlParams, $url);
        }

        Log::debug("Shopify Mock: {$this->resourceKey}: delete is not implemented, using real one");

        return parent::delete($urlParams, $url);
    }

    /**
     * @inheritDoc
     */
    public function generateUrl($urlParams = [], $customAction = null)
    {
        $url = parent::generateUrl($urlParams, $customAction);

        Log::debug("Shopify Mock: {$this->resourceKey}: generateUrl", [
            'url' => $url,
            'id' => $this->id,
            'urlParams' => $urlParams,
            'customAction' => $customAction,
        ]);

        return $url;
    }

    /**
     * @inheritDoc
     */
    public function getNextPageParams(){
        return $this->getMockStorageResource()->getNextPageParams();
    }

    /**
     * @inheritDoc
     */
    public function getPrevPageParams(){
        return $this->getMockStorageResource()->getPrevPageParams();
    }

    /* Generic implementation of mock methods */
    abstract function getMockStorageResource(): MockStorageShopifyResource;

    public function mockValidateResource() {
        if (!$this->id || !$this->getMockStorageResource()->load($this->id)) {
            throw new ApiException('Not Found', 404);
        };
    }

    public function mockValidateWriteRequest($dataArray) {}

    public function mockValidateParentResource() {}

    public function mockPost(array $dataArray = [], $url = null, $wrapData = true)
    {
        $this->mockValidateParentResource();
        $this->mockValidateWriteRequest($dataArray);

        return $this->getMockStorageResource()->post($dataArray);
    }

    public function mockGet(array $urlParams = [], $url = null, $dataKey = null)
    {
        //get multiple
        if(!$this->id) {
            return $this->getMockStorageResource()->get($urlParams)->toArray();
        }

        $this->mockValidateParentResource();
        $this->mockValidateResource();

        //get single
        return $this->getMockStorageResource()->load($this->id, $urlParams);
    }

    public function mockPut(array $dataArray = [], $url = null, $wrapData = true)
    {
        $this->mockValidateParentResource();
        $this->mockValidateResource();
        $this->mockValidateWriteRequest($dataArray);

        return $this->getMockStorageResource()->put($this->id, $dataArray);
    }

    public function mockDelete(array $urlParams = [], $url = null)
    {
        $this->mockValidateParentResource();
        $this->mockValidateResource();

        return $this->getMockStorageResource()->delete($this->id);
    }

    public function mockCount($urlParams)
    {
        return $this->getMockStorageResource()->count($urlParams);
    }

    public function mockSearch($query)
    {
        throw new \Exception('Product MockSearch is not implemented for'.$this->getResourceName());
        return $this->getMockStorageResource()->search($query);
    }
}