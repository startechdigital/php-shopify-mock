<?php

namespace PHPShopifyMock\Service;

use PHPShopify\Exception\SdkException;
use PHPShopify\ShopifyResource;
use PHPShopify\ShopifySDK as OriginalShopifySDK;
use PHPShopifyMock\Storage;
use Illuminate\Support\Facades\Log;

class ShopifySDK extends OriginalShopifySDK
{
    /** @var Storage */
    public $mockStorage;

    public function setMockStorage(Storage $mockStorage): self
    {
        $this->mockStorage = $mockStorage;
        return $this;
    }
    public function getMockStorage(): Storage
    {
        return $this->mockStorage;
    }


    /**
     * Make sure we create instance of mock resources whenever we can
     * @inheritDoc
     */
    public function __call($resourceName, $arguments)
    {
        if (!in_array($resourceName, $this->resources)) {
            if (isset($this->childResources[$resourceName])) {
                $message = "$resourceName is a child resource of " . $this->childResources[$resourceName] . ". Cannot be accessed directly.";
            } else {
                $message = "Invalid resource name $resourceName. Pls check the API Reference to get the appropriate resource name.";
            }
            throw new SdkException($message);
        }

        $resourceClassName = __NAMESPACE__ . "\\ShopifyResource\\$resourceName";
        $useShopifyMock = class_exists($resourceClassName);

        if(!$useShopifyMock) {
            Log::debug("Shopify Mock: resource {$resourceName} is not implemented, using real one");
            $resourceClassName = "PHPShopify\\$resourceName";
        }

        //If first argument is provided, it will be considered as the ID of the resource.
        $resourceID = !empty($arguments) ? $arguments[0] : null;

        //Initiate the resource object
        $resource = new $resourceClassName($resourceID);

        //set MockStorage if we are using mocked resource
        if($useShopifyMock) {
            $resource->setMockStorage($this->getMockStorage());
        }

        return $resource;
    }
}